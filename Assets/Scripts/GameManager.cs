﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;

public class GameManager : MonoBehaviour
{
    public static int CurrentLevelNumber;

    //PETER'S PARAMETERS
    public static float timePlayed;
    public static float timePlayedThisLevel;

    // Allow flags to be used
    [System.Flags]
    public enum Symbols
    {

        //The bitshift phenomenon as described in class
        //Allows all of these to be flattened in binary to create a code for which flags are checked off

        Waves = 1 << 0,
        Dot = 1 << 1,
        Square = 1 << 2,
        LargeDiamond = 1 << 3,
        SmallDiamonds = 1 << 4,
        Command = 1 << 5,
        Bomb = 1 << 6,
        Sun = 1 << 7,
        Bones = 1 << 8,
        Drop = 1 << 9,
        Face = 1 << 10,
        Hand = 1 << 11,
        Flag = 1 << 12,
        Disk = 1 << 13,
        Candle = 1 << 14,
        Wheel = 1 << 15
    }

    //A serializable STRUCT - a set of parameters that allows you to build a level with flags.
    [System.Serializable]
    public struct LevelDescription
    {
        // 
        public int Rows, Columns;
        //Creating a serializable menu where you can flag any of the members of the Symbols enum. 
        [EnumFlags]
        public Symbols UsedSymbols;
    }

    public GameObject TilePrefab;
    public float TileSpacing;

    //Initializing a level list
    public LevelDescription[] Levels = new LevelDescription[0];
    public Action HideTilesEvent;

    private Tile m_tileOne, m_tileTwo;
    private int m_cardsRemaining;



    private void Start()
    {
        LoadLevel(CurrentLevelNumber);

    }

    private void Update()
    {
        timePlayed += Time.deltaTime;
        timePlayedThisLevel += Time.deltaTime;
    }

    // Building the level and assigning required symbols. 
    private void LoadLevel(int levelNumber)
    {
        if (levelNumber != 0)
            AnalyticsEvent.Custom("BeatLevel", new Dictionary<string, object>
        {
            {"level_number", levelNumber-1},
            {"level_time_in_seconds", Mathf.RoundToInt(timePlayedThisLevel) },
            {"totaltime_seconds", Mathf.RoundToInt(timePlayed) }
        });

        //reset level time
        timePlayedThisLevel = 0;

        //Iterate through the levels list - just don't go over. 
        LevelDescription level = Levels[levelNumber % Levels.Length];

        //Get required symbols for the current level
        List<Symbols> symbols = GetRequiredSymbols(level);

        //Place the cards according to the number of rows and columns, and their spacing. This includes assigning their symbols. 
        for (int rowIndex = 0; rowIndex < level.Rows; ++rowIndex)
        {
            float yPosition = rowIndex * (1 + TileSpacing);
            for (int colIndex = 0; colIndex < level.Columns; ++colIndex)
            {
                float xPosition = colIndex * (1 + TileSpacing);
                GameObject tileObject = Instantiate(TilePrefab, new Vector3(xPosition, yPosition, 0), Quaternion.identity, this.transform);

                //Assigning the symbol for a card, once placed. Once a symbol is assigned, take it OUT of the list you initialized. 
                int symbolIndex = UnityEngine.Random.Range(0, symbols.Count);
                tileObject.GetComponentInChildren<Renderer>().material.mainTextureOffset = GetOffsetFromSymbol(symbols[symbolIndex]);
                tileObject.GetComponent<Tile>().Initialize(this, symbols[symbolIndex]);
                symbols.RemoveAt(symbolIndex);
            }
        }

        SetupCamera(level);
    }

    //Calculating the number of the specified symbols needed to fill the board
    private List<Symbols> GetRequiredSymbols(LevelDescription level)
    {
        //Makin a list, getting a total # of cards needed
        List<Symbols> symbols = new List<Symbols>();
        int cardTotal = level.Rows * level.Columns;

        //Look at all symbols in the game. Ensure there is an even number of cards. Add ONLY the used symbols to the level's symbols list
        {
            Array allSymbols = Enum.GetValues(typeof(Symbols));

            m_cardsRemaining = cardTotal;

            if (cardTotal % 2 > 0)
            {
                new ArgumentException("There must be an even number of cards");
            }

            foreach (Symbols symbol in allSymbols)
            {
                if ((level.UsedSymbols & symbol) > 0)
                {
                    symbols.Add(symbol);
                }
            }
        }

        //Ensuring the level has an appropriate amount of symbols flagged in the inspector. 
        {
            if (symbols.Count == 0)
            {
                new ArgumentException("The level has no symbols set");
            }
            if (symbols.Count > cardTotal / 2)
            {
                new ArgumentException("There are too many symbols for the number of cards.");
            }
        }

        //This method adds ADDITIONAL copies of a random pair, if there are more cards than symbol pairs. 
        {
            //How many symbols do we need to duplicate?
            int repeatCount = (cardTotal / 2) - symbols.Count;
            //if any... 
            if (repeatCount > 0)
            {
                //Copy the symbols list we already have (which at this point only has one of each required symbol) 
                List<Symbols> symbolsCopy = new List<Symbols>(symbols);

                //Create a totally new list for duplicates. 
                List<Symbols> duplicateSymbols = new List<Symbols>();

                //For each repeat necessarry... 
                for (int repeatIndex = 0; repeatIndex < repeatCount; ++repeatIndex)
                {
                    //pick a random index in the symbols copy. 
                    int randomIndex = UnityEngine.Random.Range(0, symbolsCopy.Count);
                    //Randomly PEPPER the copy into our duplicates list. 
                    duplicateSymbols.Add(symbolsCopy[randomIndex]);
                    //When something is added to Duplicate Symbols, take it out of our SymbolsCopy. 
                    symbolsCopy.RemoveAt(randomIndex);

                    //If you've emptied symbols copy, append symbols to it! Just in case the card grid is huge I guess. 
                    if (symbolsCopy.Count == 0)
                    {
                        symbolsCopy.AddRange(symbols);
                    }
                }

                //When finished, add our fattened duplicate array onto symbols. 
                symbols.AddRange(duplicateSymbols);
            }
        }

        //Then add symbols to symbols - finally giving us two of each card. 
        symbols.AddRange(symbols);

        //Return are fresh juicy List. 
        return symbols;
    }

    //I *THINK* this is what retrieves the actual visual of the symbol. 
    private Vector2 GetOffsetFromSymbol(Symbols symbol)
    {
        Array symbols = Enum.GetValues(typeof(Symbols));
        for (int symbolIndex = 0; symbolIndex < symbols.Length; ++symbolIndex)
        {
            if ((Symbols)symbols.GetValue(symbolIndex) == symbol)
            {
                return new Vector2(symbolIndex % 4, symbolIndex / 4) / 4f;
            }
        }
        Debug.Log("Failed to find symbol");
        return Vector2.zero;
    }

    //Set the camera to be able to see whatever size grid you made. 
    private void SetupCamera(LevelDescription level)
    {
        Camera.main.orthographicSize = (level.Rows + (level.Rows + 1) * TileSpacing) / 2;
        Camera.main.transform.position = new Vector3((level.Columns * (1 + TileSpacing)) / 2, (level.Rows * (1 + TileSpacing)) / 2, -10);
    }

    //What happens when you touch a tile with your grubby little hands. 
    public void TileSelected(Tile tile)
    {
        if (m_tileOne == null)
        {
            m_tileOne = tile;
            m_tileOne.Reveal();
        }
        else if (m_tileTwo == null)
        {

            //If this is the second one you're flipping, compare them. 
            m_tileTwo = tile;
            m_tileTwo.Reveal();
            if (m_tileOne.Symbol == m_tileTwo.Symbol)
            {
                StartCoroutine(WaitForHide(true, 1f));
            }
            else
            {
                //Don't let the player flip third+ tiles. 
                StartCoroutine(WaitForHide(false, 1f));
            }
        }
    }

    //Loads the next level, or Debug.Logs GAME OVER if there is none 
    private void LevelComplete()
    {
        ++CurrentLevelNumber;
        if (CurrentLevelNumber > Levels.Length - 1)
        {
            Debug.Log("GameOver");
        }
        else
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
    }

    //FLips cards back over afer a set time, and also runs logic for whether they were a match or not. 
    private IEnumerator WaitForHide(bool match, float time)
    {
        float timer = 0;
        while (timer < time)
        {
            timer += Time.deltaTime;
            if (timer >= time)
            {
                break;
            }
            yield return null;
        }
        if (match)
        {
            //PETER CODE: If tile two was a "virgin," this was a "lucky flip!"
            if (m_tileTwo.virgin == true)
                AnalyticsEvent.Custom("Blind/Lucky Match", new Dictionary<string, object>
        {
            {"level", CurrentLevelNumber},
        });
            else
                AnalyticsEvent.Custom("Known Match", new Dictionary<string, object>
        {
            {"level", CurrentLevelNumber},
        });



            Destroy(m_tileOne.gameObject);
            Destroy(m_tileTwo.gameObject);
            m_cardsRemaining -= 2;
        }
        else
        {
            m_tileOne.Hide();
            m_tileTwo.Hide();

            //If tile two has already been flipped, this was a "mistake!"

            if (m_tileTwo.virgin != true)
            {
                AnalyticsEvent.Custom("Repeat Non-Match", new Dictionary<string, object>
        {
            {"level", CurrentLevelNumber},
        });

            }
            else
            {
                //PETER CODE: If the tiles don't match, this was a "standard flip"
                AnalyticsEvent.Custom("Standard Non-Match", new Dictionary<string, object>
        {
            {"level", CurrentLevelNumber},
        });
            }

        }
        m_tileOne = null;
        m_tileTwo = null;

        if (m_cardsRemaining == 0)
        {
            LevelComplete();
        }
    }
}
